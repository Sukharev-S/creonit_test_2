import React from 'react';
import { Link } from 'react-router-dom';
import './header.css';

const Header = () => {
  return (
    <header className="header">
      <div className="header__logo">
        <h1>
        <Link to="/">API-TEST-2</Link>
        </h1>
      </div>
      <ul className="header__listItems">
        <li className="header__listItem item1">
          <Link to="/shiny">
            <img src="https://img.icons8.com/dotty/100/000000/tire.png" width="100" alt='the tire'/>
            Shiny
          </Link>
        </li>
        <li className="header__listItem item2">
          <Link to="/diski">
            <img src="https://img.icons8.com/ios/100/000000/wheel.png" width="100" alt='the disk'/>
            Diski
          </Link>
        </li>
      </ul>
      <div className="header__basket">
        <Link to="/basket" className="header__basketRef">
          <img src="https://img.icons8.com/nolan/100/000000/shopping-cart.png" width="100" alt='the shopping-cart'/>
          Basket
        </Link>
      </div>
    </header>
  )
};

export default Header;

