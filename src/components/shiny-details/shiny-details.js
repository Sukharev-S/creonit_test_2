import React, { Component } from 'react';
import ApiService from '../../services/api-service';
import './shiny-details.css';

export default class ShinyDetails extends Component {

  apiService = new ApiService();

  state = {
    shina: null
  }
  
  componentDidMount() {
    this.updateShina();
  }

  componentDidUpdate(prevProps) {
    if (this.props.shinaId !== prevProps.shinaId) {
      this.updateShina();
    }
  }

  updateShina() {
    const { shinaId } = this.props;
    if (!shinaId) return;
    this.apiService
      .getShina(shinaId)
      .then((shina) => this.setState({ shina }));
  }

  render() {
    if (!this.state.shina) {
      return <span className="itemDetails__noCard">Select a shina from a list, please</span>;
    }
    const { id, model, image, price } = this.state.shina[0];
    return (
      <div className="itemDetails__card">
        <h3 className="itemDetails__title">{model}</h3>
        <div className="itemDetails__imageWrap">
          <img className="itemDetails__image" src={image} width="350" alt='product'/>
        </div>
        <span className="itemDetails__price">${price}</span>
        <button className="itemDetails__addBasket"
                onClick={() => { 
                  this.apiService.addToBasket(id);
                  document.querySelector('.itemDetails__addBasketMessage').classList.add('itemDetails__addBasketMessage--active');
                  setTimeout(() => { document.querySelector('.itemDetails__addBasketMessage').classList.remove('itemDetails__addBasketMessage--active'); }, 500);
                  }}>
          To buy
        </button>
        <span className="itemDetails__addBasketMessage"> The good added to cart ! </span>
      </div>
    )
  }
}
