export default class ApiService {
  
  _apiBase = 'http://vue-tests.dev.creonit.ru/api';   // приватная часть кода

  _transformShiny = (shina) => {
    return {
      id: shina.id,      
      model: shina.title,
      image: shina.image,
      imagePreview: shina.image_preview,
      price: shina.price        
    }
  }

  _transformDiski = (disk) => {
    return {
      id: disk.id,
      model: disk.title,
      image: disk.image,
      imagePreview: disk.image_preview,
      price: disk.price
    }
  }

  _transformBasket = (list) => {
    return {
      amount: list.amount,
      total: list.total,
      id: list.product.id,
      model: list.product.title,
      image: list.product.image,
      imagePreview: list.product.image_preview,
      price: list.product.price
    }
  }

  async getResource(url) {
    const res = await fetch(`${this._apiBase}${url}`);   //Fetch работает с промисами
    if (!res.ok) {
      throw new Error(`Could not fetch ${url} received ${res.status}`)
    }
    return await res.json();
  }

  async sendResource(url) {
    const res = await fetch(`${this._apiBase}${url}`, { method: 'post' });
    if (!res.ok) {
      throw new Error(`Could not fetch ${url} received ${res.status}`)
    }
    return await res.json();
  }

  getAllShiny = async () => { 
    const res = await this.getResource(`/catalog/shiny`);
    return res.items.map(this._transformShiny);         // возвращает массив всех шин
  }

  getShina = async (id) => {
    const shina = await this.getAllShiny()
    return shina.filter(item => item.id === id);       // возвращает шину по номеру
  }

  getAllDiski = async () => { 
    const res = await this.getResource(`/catalog/diski`);
    return res.items.map(this._transformDiski);         // возвращает массив всех дисков
  }

  getDisk = async (id) => {
    const disk = await this.getAllDiski()
    return disk.filter(item => item.id === id);       // возвращает диск по номеру
  }

  getBasket = async () => { 
    const res = await this.getResource(`/cart/list`);
    return res.list.map(this._transformBasket);         // возвращает содержимое корзины
  }

  addToBasket = async (id) => {
    await this.sendResource(`/cart/product/${id}`);     // добавляет товар в корзину
  }
}